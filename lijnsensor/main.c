#include <msp430.h> 

#define midden  BIT1
#define rechts  BIT2
#define links   BIT3

int afslagL = 0;        //om het aantal afslagen van links te tellen
int afslagR = 0;        // om het aantal afslagen van rechts te tellen
/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	P1DIR &= ~(midden | rechts | links);        //activeert de drie pinnen
	while(1)
	{
        if ((P1IN & midden) != 0)                   //of er in het midden een zwarte lijn gedetecteerd wordt
        {
            if ((P1IN & rechts) != 0)               //of er rechts een zwarte lijn wordt gedetecteerd
            {
                if ((P1IN & links) != 0)            // of er links een zwarte lijn wordt gedetecteerd
                {
                    afslagL++;                      //Er is een afslag links gedetecteerd
                    afslagR++;                      //er is een afslag rechts gedetecteerd
                }
                else                                // als er niet een lijn links ligt
                {
                    afslagR++;                      //er is een afslag rechts gedetecteerd
                }
            }
            else
            {
                if ((P1IN & links) != 0)
                {
                    afslagL++;
                }
            }
        }
        if((P1IN & midden) == 0)                                        //als er geen zwarte lijn in het midden is
        {
            if ((P1IN & rechts) != 0)
            {
                if ((P1IN & links) != 0)
                {
                    afslagR++;
                    afslagL++;
                }
            }
            else
            {
                if ((P1IN & links) != 0)
                {
                    afslagL++;
                }
            }
        }
	}
	return 0;
}
